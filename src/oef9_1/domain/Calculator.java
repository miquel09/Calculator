package oef9_1.domain;

public class Calculator {

    private String display, calc;
    private boolean calculated, mathOperation;

    public Calculator() {
        display = "";
        calc = "";
    }

    public String getDisplay() {
        return display;
    }

    public String getCalc() {
        calc += ".0";
        return calc;
    }

    /*
    This methods uses try catch to set a boolean to false if the parameter
    entered isn't numerical. This will add the math opperation to the calc String.
    If the value is numerical .0 is added behind it to make it a double.
     */
    /**
     * Adds the pressed button value to the strings.
     *
     * @param val The value to be added.
     */
    public void addToString(String val) {
        boolean isInt = true;
        try {
            Integer.parseInt(val);
        } catch (NumberFormatException e) {
            isInt = false;
        }
        if (isInt) {
            calc += val;
        } else if (!isInt && !mathOperation) {
            calc += ".0" + val; 
        }
        display += val;
    }

    /**
     * Checks if the display needs to be cleared after a calculation. Clears the
     * display if a calculation has been excecuted and a numerical value has
     * been pressed.
     *
     * @param val The value to add to the srtings.
     */
    public void doCheck(String val) {
        if (calculated) {
            clearDisplay();
        }
        addToString(val);
    }

    public void setCalculated(boolean bool) {
        calculated = bool;
    }
    
    public void setMathOp(boolean opp)  {
        mathOperation = opp;
    }

    public void clearDisplay() {
        display = "";
        calc = "";
        calculated = false;
    }

}
