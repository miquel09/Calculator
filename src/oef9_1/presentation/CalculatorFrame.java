/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package oef9_1.presentation;

import engine.presentation.ContentFrame;
import engine.presentation.PresentationConstants;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import javax.swing.BoxLayout;
import javax.swing.JButton;
import static javax.swing.JFrame.EXIT_ON_CLOSE;
import javax.swing.JPanel;
import javax.swing.JTextField;
import oef9_1.businesslogic.Logic;
import oef9_1.domain.Calculator;

public class CalculatorFrame{

    private JTextField inputField;
    private JButton btn, btnCalc, btnC;
    private final Calculator calc;
    private final Logic logic;

    public CalculatorFrame(Logic logic) {
        
        calc = new Calculator();
        this.logic = logic;
        setupFrame();
    }

    private void setupFrame() {
        ContentFrame frame = new ContentFrame(185,200, EXIT_ON_CLOSE, false);
        frame.makeContentPane(5, 5, PresentationConstants.BORDER_LAYOUT);
        JPanel contentPane = frame.getContentPane();

        JPanel searchPanel = createSearchPanel();
        contentPane.add(searchPanel, BorderLayout.NORTH);

        JPanel infoPanel = createInfoPanel();
        contentPane.add(infoPanel, BorderLayout.CENTER);
        frame.setVisible(true);
    }

    private JPanel createSearchPanel() {
        JPanel searchPanel = new JPanel();
        searchPanel.setLayout(new BoxLayout(searchPanel, BoxLayout.X_AXIS));

        inputField = new JTextField(10);
        inputField.setSize(new Dimension(6, 20));
        inputField.setHorizontalAlignment(JTextField.RIGHT);
        inputField.setEditable(false);
        inputField.setBackground(Color.white);
        searchPanel.add(inputField);

        return searchPanel;
    }

    private JPanel createInfoPanel() {
        JPanel numberPanel = new JPanel();
        GridLayout grid = new GridLayout(4, 4);
        numberPanel.setLayout(grid);

        createNumberGrid(numberPanel);

        return numberPanel;
    }

    private void createNumberGrid(JPanel p) {
        //row 1
        makeNumBtn("7", btn, p);
        makeNumBtn("8", btn, p);
        makeNumBtn("9", btn, p);
        makeActBtn("+", btn, p);

        //row 2
        makeNumBtn("4", btn, p);
        makeNumBtn("5", btn, p);
        makeNumBtn("6", btn, p);
        makeActBtn("-", btn, p);

        //row3
        makeNumBtn("1", btn, p);
        makeNumBtn("2", btn, p);
        makeNumBtn("3", btn, p);
        makeActBtn("*", btn, p);

        //row 4
        makeNumBtn("0", btn, p);
        makeActBtn("/", btn, p);

        btnC = new JButton("C");
        p.add(btnC);
        btnC.addActionListener((ActionEvent ae) -> {
            calc.clearDisplay();
            inputField.setText(calc.getDisplay());
        });

        btnCalc = new JButton("=");
        p.add(btnCalc);
        btnCalc.addActionListener((ActionEvent ae) -> {
            inputField.setText("" + logic.calc(calc.getCalc()));
            calc.setCalculated(true);
        });
    }

    private void makeNumBtn(String val, JButton btn, JPanel p) {
        btn = new JButton(val);
        p.add(btn);
        btn.addActionListener((ActionEvent ae) -> {
            calc.doCheck(val);
            inputField.setText(calc.getDisplay());
            calc.setMathOp(false);
        });
    }

    private void makeActBtn(String txt, JButton btn, JPanel p) {
        btn = new JButton(txt);
        p.add(btn);
        btn.addActionListener((ActionEvent ae) -> {
            calc.addToString(txt);
            calc.setCalculated(false);
            inputField.setText(calc.getDisplay());
            calc.setMathOp(true);
        });
    }
}
