/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package oef9_1;

import oef9_1.businesslogic.Logic;
import oef9_1.presentation.CalculatorFrame;

/**
 *
 * @author miquel
 */
public class Oef9_1 {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        Logic logic = new Logic();
        CalculatorFrame frame = new CalculatorFrame(logic);
    }

}
