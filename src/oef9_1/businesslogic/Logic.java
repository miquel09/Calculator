package oef9_1.businesslogic;

import oef9_1.presentation.CalculatorFrame;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.script.ScriptEngine;
import javax.script.ScriptEngineManager;

public class Logic {

    private final ScriptEngine engine;

    public Logic() {
        ScriptEngineManager mgr = new ScriptEngineManager();
        engine = mgr.getEngineByName("JavaScript");
    }

    public double calc(String math) {
        try {
            return eval(math);
        } catch (Exception ex) {
            Logger.getLogger(CalculatorFrame.class.getName()).log(Level.SEVERE, null, ex);
        }
        return 0.0;
    }

    public final double eval(String math) throws Exception {
        System.out.println(math);
        
        return (double) engine.eval(math);
    }
}
