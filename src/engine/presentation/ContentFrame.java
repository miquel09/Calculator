package engine.presentation;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.GridLayout;
import javax.swing.JFrame;
import javax.swing.JPanel;

public class ContentFrame extends JFrame implements PresentationConstants{
    private JPanel contentPane;
    
    /**
     * The constructor to make a frame with a <code>width</code>, <code>height</code>, 
     * <code>DefaultCloseOpperation</code> and a <code>resizeability</code>.
     * 
     * @param WIDTH. The width of the frame.
     * @param HEIGHT. The height of the frame.
     * @param defaultCloseOpperation. The default Close Opperation of the frame. 
     * @param resizeable. A boolean to set the resizability of the frame. False 
     * if it can't be resized, true if it can be resized.
     */
    public ContentFrame(int WIDTH, int HEIGHT, int defaultCloseOpperation, boolean resizeable)  {
        this.setDefaultCloseOperation(defaultCloseOpperation);
        this.setSize(new Dimension(WIDTH, HEIGHT));
        this.setResizable(resizeable);
    }
    
    /**
     * The constructor to make a frame with a <code>width</code>, <code>height</code>, 
     * <code>DefaultCloseOpperation</code> and a <code>resizeability</code>.
     * 
     * @param WIDTH. The width of the frame.
     * @param HEIGHT. The height of the frame.
     * @param defaultCloseOpperation. The default Close Opperation of the frame. 
     * @param title. The title of the frame.
     * @param resizeable. A boolean to set the resizability of the frame. False 
     * if it can't be resized, true if it can be resized.
     */
    public ContentFrame(int WIDTH, int HEIGHT, int defaultCloseOpperation, boolean resizeable, String title)  {
        this.setDefaultCloseOperation(defaultCloseOpperation);
        this.setSize(new Dimension(WIDTH, HEIGHT));
        this.setResizable(resizeable);
        this.setTitle(title);
    }
    
    /**
     * Makes the contentPane of the Frame with a layout and default values.
     * 
     * @param layout. The layout you want GRID_LAYOUT or BORDER_LAYOUT.
     */
    public void makeContentPane(int layout)  {
        switch(layout)  {
            case NULL_LAYOUT:
                makeContentPane(0, 0, 0, 0, layout);
            break;  
            case BORDER_LAYOUT:
                makeContentPane(0, 0, 0, 0, layout);
                break;
            case GRID_LAYOUT:
                makeContentPane(1, 0, 0, 0, layout);
                break;
        }
    }
    
    /**
     * Makes the contentPane of the Frame with rows and columns or hgap and vgap.
     * 
     * @param hgap_rows The hgap if BORDER_LAYOUT or the rows if GRID_LAYOUT
     * @param vgap_cols The vgap if BORDER_LAYOUT or the coulumns if GRID_LAYOUT
     * @param layout. The layout you want GRID_LAYOUT or BORDER_LAYOUT.
     */
    public void makeContentPane(int hgap_rows, int vgap_cols, int layout)  {
        switch(layout)  {
            case BORDER_LAYOUT:
                makeContentPane(0, 0, hgap_rows, vgap_cols, layout);
            break;
            
            case GRID_LAYOUT:
               makeContentPane(hgap_rows, vgap_cols, 0, 0, layout);
            break;
        }
    }
    
    /**
     * Makes the contentPane of the Frame with rows, columns, hgap, vgap and a layout.
     * 
     * @param rows The rows for a grid layout.
     * @param cols The columns for a grid layout.
     * @param hgap The hgap of the layout.
     * @param vgap The vgap of the layout.
     * @param layout. The layout you want GRID_LAYOUT or BORDER_LAYOUT.
     */
    public void makeContentPane(int rows, int cols, int hgap, int vgap, int layout)  {
        switch(layout)  {
            case NULL_LAYOUT:
                setupNullFrame();
            break;  
            
            case BORDER_LAYOUT:
                setupBorderFrame(hgap, vgap);
            break;
            
            case GRID_LAYOUT:
                setupGridFrame(rows, cols, hgap, vgap);
            break;
        }
       
    }
    
    private void setupGridFrame(int rows, int cols, int hgap, int vgap){
        JPanel contentPane = new JPanel();
        contentPane.setLayout(new GridLayout(rows, cols, hgap, vgap));
        this.setContentPane(contentPane);
        this.contentPane = contentPane;
    }
    
    private void setupBorderFrame(int hgap, int vgap) {
        JPanel contentPane = new JPanel();
        contentPane.setLayout(new BorderLayout(hgap, vgap));
        this.setContentPane(contentPane);
        this.contentPane = contentPane;
    }
    
    private void setupNullFrame()  {
        JPanel contentPane = new JPanel();
        contentPane.setLayout(null);
        this.setContentPane(contentPane);
        this.contentPane = contentPane;
    }
    
    /**
     * Returns the Content Pane JPanel of the frame.
     * @return a JPanel.
     */
    @Override
    public JPanel getContentPane()  {
        return contentPane;
    }
}
