package engine.presentation;

public interface PresentationConstants {
    int NULL_LAYOUT     = 0;
    int BORDER_LAYOUT   = 1;
    int GRID_LAYOUT     = 2;
}
